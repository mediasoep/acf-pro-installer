<?php namespace MediaSoep\ACFProInstaller\Test;

use MediaSoep\ACFProInstaller\RemoteFilesystem;

class RemoteFilesystemTest extends \PHPUnit_Framework_TestCase
{
    protected $io;
    protected $config;

    public function testExtendsComposerRemoteFilesystem()
    {
        $this->assertInstanceOf(
            'Composer\Util\RemoteFilesystem',
            new RemoteFilesystem('', $this->io)
        );
    }

    public function testCopyUsesAcfFileUrl()
    {
        $acfFileUrl = 'file://' . __FILE__;
        $rfs = new RemoteFilesystem($acfFileUrl, $this->io);
        $file = tempnam(sys_get_temp_dir(), 'pb');

        $this->assertTrue(
            $rfs->copy('http://example.org', 'does-not-exist', $file)
        );
        $this->assertFileExists($file);
        unlink($file);
    }

    // Inspired by testCopy of Composer

    protected function setUp()
    {
        $this->io = $this->getMock('Composer\IO\IOInterface');
    }
}
